package asynchWithTasks.task;

import asynchWithTasks.seq.Body;
import asynchWithTasks.seq.Boundary;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.RecursiveTask;

public class UpdateBodyTask extends RecursiveTask<List<Body>> {

    private final List<Body> bodies;
    private final Boundary bounds;
    private final double dt;

    public UpdateBodyTask(List<Body> bodies, Boundary bounds, double dt){
        this.bodies = new LinkedList<>(bodies);
        this.bounds = bounds;
        this.dt = dt;
    }

    @Override
    protected List<Body> compute() {
        List<RecursiveTask<Body>> forks = new LinkedList<>();
        List<Body> updatedBodyList = new LinkedList<>();

        for(Body body: bodies){
            ComputeVelocityTask task = new ComputeVelocityTask(bodies, body, dt);
            forks.add(task);
            task.fork();
        }

        for(RecursiveTask<Body> task: forks){
            updatedBodyList.add(task.join());
           // System.out.println("done join velocity");
        }

        forks.clear();

        for(Body body: updatedBodyList){
            ComputePositionTask task = new ComputePositionTask(bounds, body, dt);
            forks.add(task);
            task.fork();
        }

        updatedBodyList.clear();

        for(RecursiveTask<Body> task: forks){
            updatedBodyList.add(task.join());
            //System.out.println("done join position");
        }

        return updatedBodyList;
    }
}
