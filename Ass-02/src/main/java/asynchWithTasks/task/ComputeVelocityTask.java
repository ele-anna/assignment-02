package asynchWithTasks.task;

import asynchWithTasks.seq.Body;
import asynchWithTasks.seq.InfiniteForceException;
import asynchWithTasks.seq.V2d;

import java.util.List;
import java.util.concurrent.RecursiveTask;

public class ComputeVelocityTask extends RecursiveTask<Body> {

    private final List<Body> bodies;
    private final Body body;
    private final double dt;

    public ComputeVelocityTask(List<Body> bodies, Body body, double dt) {
        this.bodies = bodies;
        this.body = body;
        this.dt = dt;
    }

    @Override
    protected Body compute() {
        V2d totalForce = new V2d(0, 0);

        for(Body otherBody : bodies){
            if(!otherBody.equals(body)){
                try {
                    totalForce.sum(body.computeRepulsiveForceBy(otherBody));
                } catch (InfiniteForceException e) {}
            }
        }

        totalForce.sum(body.getCurrentFrictionForce());
        V2d acc = new V2d(totalForce).scalarMul(1.0 / body.getMass());
        body.updateVelocity(acc, dt);

        return body;
    }
}
