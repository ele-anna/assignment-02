package asynchWithTasks.task;

import asynchWithTasks.seq.Body;
import asynchWithTasks.seq.Boundary;

import java.util.concurrent.RecursiveTask;

public class ComputePositionTask  extends RecursiveTask<Body>  {

    private final Body body;
    private final Boundary bounds;
    private final double dt;

    public ComputePositionTask(Boundary bounds, Body body, double dt) {
        this.body = body;
        this.bounds = bounds;
        this.dt = dt;
    }

    @Override
    protected Body compute() {
        body.updatePos(dt);
        body.checkAndSolveBoundaryCollision(bounds);
        return body;
    }
}

