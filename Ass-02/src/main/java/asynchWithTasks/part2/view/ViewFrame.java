package asynchWithTasks.part2.view;


import asynchWithTasks.part2.controller.InputListener;
import asynchWithTasks.seq.Body;
import asynchWithTasks.seq.Boundary;
import asynchWithTasks.seq.P2d;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.LinkedList;
import java.util.List;

public class ViewFrame extends JFrame implements ActionListener {

    private final JButton startButton;
    private final JButton stopButton;
    private final JTextField virtualTime;
    private final VisualiserPanel panel;
    private final List<InputListener> listeners;

    public ViewFrame(int width, int height){
        super("Bodies Simulation");
        super.setSize(width, height);
        super.setResizable(false);

        this.listeners = new LinkedList<>();

        this.startButton = new JButton("start");
        this.stopButton = new JButton("stop");
        this.virtualTime = new JTextField(5);
        this.virtualTime.setText("0");
        this.virtualTime.setEditable(false);

        JPanel commandPanel = new JPanel();
        commandPanel.add(this.startButton);
        commandPanel.add(this.stopButton);
        commandPanel.add(new JLabel("Virtual time: "));
        commandPanel.add(this.virtualTime);


        JPanel controlPanel = new JPanel();
        controlPanel.setLayout(new BorderLayout());

        super.setContentPane(controlPanel);

        this.panel = new VisualiserPanel(width, height);
        super.getContentPane().add(panel);
        super.addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent ev){
                System.exit(-1);
            }
            public void windowClosed(WindowEvent ev){
                System.exit(-1);
            }
        });

        controlPanel.add(BorderLayout.SOUTH, commandPanel);
        controlPanel.add(BorderLayout.CENTER, panel);

        this.startButton.addActionListener(this);
        this.stopButton.addActionListener(this);
        super.setDefaultCloseOperation(EXIT_ON_CLOSE);

    }

    public void addListener(InputListener l){
        listeners.add(l);
    }

    private void notifyStarted(){
        for (InputListener l: listeners){
            this.startButton.setEnabled(false);
            l.start();
        }
    }

    private void notifyStopped(){
        for (InputListener l: listeners){
            this.stopButton.setEnabled(false);
            l.stop();
        }
    }

    public void update(List<Body> bodies, double vt, Boundary bounds){
        try {
            SwingUtilities.invokeLater(() -> {
                panel.updateImage(bodies, bounds);
                this.updateVirtualTime(vt);
                repaint();
            });
        } catch (Exception ex) {}
    };

    private void updateVirtualTime(double vt){
        SwingUtilities.invokeLater(() -> {
            String time = String.format("%.2f", vt);
            virtualTime.setText(time);
        });
    }

    public void finishComputation() {
        SwingUtilities.invokeLater(() -> {
            stopButton.setEnabled(false);
        });
    }

    public void updateScale(double k){panel.updateScale(k);}

    @Override
    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand();
        switch (command){
            case "start":
                notifyStarted();
                break;
            case "stop":
                notifyStopped();
                break;
        }
    }

    public static class VisualiserPanel extends JPanel implements KeyListener {

        private List<Body> bodies;
        private Boundary bounds;

        private double scale = 1;

        private long dx;
        private long dy;

        public VisualiserPanel(int w, int h){
            setSize(w,h);
            dx = w/2 - 20;
            dy = h/2 - 20;
            this.addKeyListener(this);
            setFocusable(true);
            setFocusTraversalKeysEnabled(false);
            requestFocusInWindow();
        }

        public void paint(Graphics g){
            if (bodies != null) {
                Graphics2D g2 = (Graphics2D) g;

                g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                        RenderingHints.VALUE_ANTIALIAS_ON);
                g2.setRenderingHint(RenderingHints.KEY_RENDERING,
                        RenderingHints.VALUE_RENDER_QUALITY);
                g2.clearRect(0,0,this.getWidth(),this.getHeight());


                int x0 = getXcoord(bounds.getX0());
                int y0 = getYcoord(bounds.getY0());

                int wd = getXcoord(bounds.getX1()) - x0;
                int ht = y0 - getYcoord(bounds.getY1());

                g2.drawRect(x0, y0 - ht, wd, ht);

                bodies.forEach( b -> {
                    P2d p = b.getPos();
                    int radius = (int) (10*scale);
                    if (radius < 1) {
                        radius = 1;
                    }
                    g2.drawOval(getXcoord(p.getX()),getYcoord(p.getY()), radius, radius);
                });
                g2.drawString("Bodies: " + bodies.size()  + " (UP for zoom in, DOWN for zoom out)", 2, 20);
            }
        }

        private int getXcoord(double x) {
            return (int)(dx + x*dx*scale);
        }

        private int getYcoord(double y) {
            return (int)(dy - y*dy*scale);
        }

        public void updateImage(List<Body> bodies, Boundary bounds){
            this.bodies = bodies;
            this.bounds = bounds;
        }

        public void updateScale(double k) {
            scale *= k;
        }

        @Override
        public void keyPressed(KeyEvent e) {
            if (e.getKeyCode() == 38){  		/* KEY UP */
                scale *= 1.1;
            } else if (e.getKeyCode() == 40){  	/* KEY DOWN */
                scale *= 0.9;
            }
        }

        public void keyReleased(KeyEvent e) {}
        public void keyTyped(KeyEvent e) {}
    }
}
