package asynchWithTasks.part2.view;

import asynchWithTasks.part2.controller.InputListener;
import asynchWithTasks.seq.Body;
import asynchWithTasks.seq.Boundary;

import java.util.List;

public class View {

    private final ViewFrame frame;

    public View(int width, int height){
        this.frame = new ViewFrame(width, height);
    }

    public void display(){
        javax.swing.SwingUtilities.invokeLater(() -> frame.setVisible(true));
    }

    public void addListener(InputListener l){
        this.frame.addListener(l);
    }

    public void update(List<Body> bodies, double vt, Boundary bounds){
        this.frame.update(bodies, vt, bounds);
    }

    public void notifyFinishComputation(){ this.frame.finishComputation(); }


}
