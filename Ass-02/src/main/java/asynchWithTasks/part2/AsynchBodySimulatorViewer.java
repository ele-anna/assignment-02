package asynchWithTasks.part2;

import asynchWithTasks.part2.controller.implementation.Controller;
import asynchWithTasks.part2.view.View;

public class AsynchBodySimulatorViewer {

    private static final int STEPNUMBER = 2000;

    public static void main(String[] args){
        View viewer = new View(620, 620);
        viewer.display();

        Service model = new Service(STEPNUMBER, viewer);

        Controller controller = new Controller(model);
        viewer.addListener(controller);
    }
}
