package asynchWithTasks.part2.controller.implementation;

import asynchWithTasks.part2.controller.InputListener;
import asynchWithTasks.part2.Service;

public class Controller implements InputListener {

    private final Service model;

    public Controller(Service model) {
        this.model = model;
    }

    @Override
    public void start() {
        this.model.start();
    }

    @Override
    public void stop() {
        try {
            this.model.stopComputation();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
