package asynchWithTasks.part2.controller;

public interface InputListener {

    void start();

    void stop();
}
