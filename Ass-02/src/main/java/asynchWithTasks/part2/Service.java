package asynchWithTasks.part2;

import asynchWithTasks.task.UpdateBodyTask;
import asynchWithTasks.part2.view.View;
import asynchWithTasks.seq.Body;
import asynchWithTasks.seq.Boundary;
import asynchWithTasks.seq.P2d;
import asynchWithTasks.seq.V2d;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.TimeUnit;

public class Service extends Thread{

    private static final double DT = 0.001;

    private List<Body> bodies;
    private Boundary bounds;
    private final int stepNumber;
    private final ForkJoinPool executor;
    private double vt;
    private final View view;

    public Service(int stepNumber, View view) {
        this.stepNumber = stepNumber;
        this.executor = new ForkJoinPool();
        this.vt = 0.00;
        this.view = view;

//       this.testBodySet1_two_bodies();
//       this.testBodySet2_three_bodies();
 //       this.testBodySet3_some_bodies();
        this.testBodySet4_many_bodies();
    }

    @Override
    public void run() {
        for(int i = 0; i < this.stepNumber; i++) {
            try {
                // System.out.println("------------ ITERATION " + i + "------------");
                this.bodies = executor.invoke(new UpdateBodyTask(bodies, bounds, DT));
                this.vt = vt + DT;
                this.view.update(bodies, vt, bounds);

            } catch(Exception e) {
                //silently ignore
            }
        }
        if (!executor.isShutdown()) {
            executor.shutdown();
            try {
                executor.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS);
            } catch (InterruptedException e) {
                //silently ignore
            }
        }

        this.view.notifyFinishComputation();

       // System.out.println("----------- DONE COMPUTATION ------------");
    }

    public void stopComputation() throws InterruptedException {
        executor.shutdownNow();
        this.interrupt();
    }


    private void testBodySet1_two_bodies() {
        bounds = new Boundary(-4.0, -4.0, 4.0, 4.0);
        bodies = new LinkedList<>();
        bodies.add(new Body(0, new P2d(-0.1, 0), new V2d(0,0), 1));
        bodies.add(new Body(1, new P2d(0.1, 0), new V2d(0,0), 2));
    }

    private void testBodySet2_three_bodies() {
        bounds = new Boundary(-1.0, -1.0, 1.0, 1.0);
        bodies = new LinkedList<>();
        bodies.add(new Body(0, new P2d(0, 0), new V2d(0,0), 10));
        bodies.add(new Body(1, new P2d(0.2, 0), new V2d(0,0), 1));
        bodies.add(new Body(2, new P2d(-0.2, 0), new V2d(0,0), 1));
    }

    private void testBodySet3_some_bodies() {
        bounds = new Boundary(-4.0, -4.0, 4.0, 4.0);
        int nBodies = 100;
        Random rand = new Random(System.currentTimeMillis());
        bodies = new LinkedList<>();
        for (int i = 0; i < nBodies; i++) {
            double x = bounds.getX0()*0.25 + rand.nextDouble() * (bounds.getX1() - bounds.getX0()) * 0.25;
            double y = bounds.getY0()*0.25 + rand.nextDouble() * (bounds.getY1() - bounds.getY0()) * 0.25;
            Body b = new Body(i, new P2d(x, y), new V2d(0, 0), 10);
            bodies.add(b);
        }
    }

    private void testBodySet4_many_bodies() {
        bounds = new Boundary(-6.0, -6.0, 6.0, 6.0);
        int nBodies = 1000;
        Random rand = new Random(System.currentTimeMillis());
        bodies = new LinkedList<>();
        for (int i = 0; i < nBodies; i++) {
            double x = bounds.getX0()*0.25 + rand.nextDouble() * (bounds.getX1() - bounds.getX0()) * 0.25;
            double y = bounds.getY0()*0.25 + rand.nextDouble() * (bounds.getY1() - bounds.getY0()) * 0.25;
            Body b = new Body(i, new P2d(x, y), new V2d(0, 0), 10);
            bodies.add(b);
        }
    }
}
