package asynchWithTasks.part1;

public class AsynchBodySimulator {

    private static final int STEPNUMBER = 500;

    public static void main(String[] args){
        long startTime = System.currentTimeMillis();

        Service service = new Service(STEPNUMBER);
        try {
            service.compute();
            long finishTime = System.currentTimeMillis();
            System.out.println("Time elapsed: "+ (finishTime - startTime));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
