package eventDriven.application;

import commonElementsProjectAnalyzer.view.View;
import eventDriven.application.controller.ControllerImpl;

public class Application {

    public static void main(String[] args) {
        ControllerImpl controllerImpl = new ControllerImpl();
        View view = new View(controllerImpl, 620, 620);
        controllerImpl.setView(view);
        view.display();
    }
}
