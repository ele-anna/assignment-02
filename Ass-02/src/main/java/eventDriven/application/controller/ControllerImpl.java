package eventDriven.application.controller;

import commonElementsProjectAnalyzer.controller.Controller;
import commonElementsProjectAnalyzer.view.View;
import eventDriven.parserLib.ProjectAnalyzer;
import eventDriven.parserLib.ProjectAnalyzerImpl;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.eventbus.MessageConsumer;

public class ControllerImpl implements Controller {

    private static final String TOPIC = "project-analyze";
    private final Vertx vertx;
    private View view;
    private MessageConsumer<String> consumer;

    public ControllerImpl() {
        VertxOptions options = new VertxOptions();
        options.setBlockedThreadCheckInterval(1000 * 60);
        this.vertx = Vertx.vertx(options);
    }

    @Override
    public void setView(View view) {
        this.view = view;
    }

    @Override
    public void startAnalyzeProject(String projectPath) {
        this.log("start analyzer");

        this.consumer = vertx.eventBus().consumer(TOPIC, message -> {
            if (message.body().equals("finish")) {
                this.view.notifyFinishComputation();
            } else {
                this.view.showProjectElem(message.body());
            }
        });

        ProjectAnalyzer projectAnalyzer = new ProjectAnalyzerImpl(vertx);
        vertx.deployVerticle(new AnalyzeProjectVerticle(projectAnalyzer, projectPath, TOPIC));
    }

    @Override
    public void notifyStop() {
        vertx.deploymentIDs().forEach(vertx::undeploy);
        if (consumer != null && consumer.isRegistered()) {
            consumer.unregister();
        }
        this.log("stop analyzer");
    }

    private void log(String message) {
        System.out.println("[Controller] " + Thread.currentThread() + message);
    }

}
