package eventDriven.application.controller;

import eventDriven.parserLib.ProjectAnalyzer;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.VertxException;

public class AnalyzeProjectVerticle extends AbstractVerticle {

    private final ProjectAnalyzer projectAnalyzer;
    private final String projectSrcPath;
    private final String topic;

    public AnalyzeProjectVerticle(ProjectAnalyzer projectAnalyzer, String projectSrcPath, String topic) {
        this.projectAnalyzer = projectAnalyzer;
        this.projectSrcPath = projectSrcPath;
        this.topic = topic;
    }

    @Override
    public void start() throws Exception {
        log("start analyze");
        try {
            projectAnalyzer.analyzeProject(this.projectSrcPath, this.topic);
        }catch(VertxException e){
            //silently ignore
        }
    }

    @Override
    public void stop() throws Exception {
        this.log("stopped");
    }

    private void log(String message) {
        System.out.println("[AnalyzeProjectVerticle] " + Thread.currentThread() + " " + message);
    }
}
