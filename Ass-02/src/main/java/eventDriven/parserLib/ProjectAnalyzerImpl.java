package eventDriven.parserLib;

import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.FieldDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.body.VariableDeclarator;
import com.google.common.io.Files;
import commonElementsProjectAnalyzer.collectors.ClassOrInterfaceCollector;
import commonElementsProjectAnalyzer.reports.*;
import commonElementsProjectAnalyzer.reports.implementation.*;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.EventBus;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.stream.Collectors;

public class ProjectAnalyzerImpl implements ProjectAnalyzer {

    private final Vertx vertx;

    public ProjectAnalyzerImpl(Vertx vertx) {
        this.vertx = vertx;
    }

    @Override
    public Future<InterfaceReport> getInterfaceReport(String srcInterfacePath) {
        return vertx.executeBlocking(promise -> {
            try {
                CompilationUnit cu = StaticJavaParser.parse(new File(srcInterfacePath));
                List<String> interfaceNames = new LinkedList<>();

                new ClassOrInterfaceCollector().visit(cu, interfaceNames);
                InterfaceReport interfaceReport = this.getInterfaceInformation(cu.getInterfaceByName(interfaceNames.get(0)).get(), srcInterfacePath);
                promise.complete(interfaceReport);
            } catch (FileNotFoundException | IllegalArgumentException e) {
                promise.fail(e.getMessage());
            }
        });
    }

    @Override
    public Future<List<ClassReport>> getClassReport(String srcClassPath) {
        return vertx.executeBlocking(promise ->{
            try {
                CompilationUnit cu = StaticJavaParser.parse(new File(srcClassPath));
                List<String> classNames = new LinkedList<>();
                List<ClassReport> classReports = new LinkedList<>();

                new ClassOrInterfaceCollector().visit(cu, classNames);

                classNames.forEach(name -> {
                    try {
                        cu.getLocalDeclarationFromClassname(name).forEach(classDeclaration -> {
                            classReports.add(this.getClassInformation(classDeclaration, srcClassPath));
                        });
                    } catch (NoSuchElementException e) {
                        classReports.add(this.getClassInformation(cu.getClassByName(name).get(), srcClassPath));
                    }
                });

                promise.complete(classReports);

            } catch (FileNotFoundException | IllegalArgumentException e) {
                promise.fail(e.getMessage());
            }
        });
    }

    @Override
    public Future<PackageReport> getPackageReport(String srcPackagePath) {
        return vertx.executeBlocking(promise -> {
            File folder = new File(srcPackagePath);
            if (folder.isDirectory()) {
                List<String> files = new LinkedList<>();
                List<ClassReport> allClasses = new LinkedList<>();
                List<InterfaceReport> allInterfaces = new LinkedList<>();

                this.collectJavaFilesInsideFolder(folder, files);

                files.forEach(classOrInterfacePath -> {
                    try {
                        CompilationUnit cu = StaticJavaParser.parse(new File(classOrInterfacePath));
                        List<String> classOrInterfaceNames = new LinkedList<>();
                        new ClassOrInterfaceCollector().visit(cu, classOrInterfaceNames);

                        classOrInterfaceNames.forEach(name -> {
                            if (cu.getClassByName(name).isPresent()) {
                                try {
                                    cu.getLocalDeclarationFromClassname(name).forEach(classDeclaration ->
                                            allClasses.add(this.getClassInformation(classDeclaration, classOrInterfacePath)));
                                } catch (NoSuchElementException e) {
                                    allClasses.add(this.getClassInformation(cu.getClassByName(name).get(), classOrInterfacePath));
                                }
                            } else if (cu.getInterfaceByName(name).isPresent()){
                                allInterfaces.add(this.getInterfaceInformation(cu.getInterfaceByName(name).get(), classOrInterfacePath));
                            }
                        });
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                });
                PackageReport packageReport = new PackageReportImpl(
                        srcPackagePath.replaceAll("src/main/java/","").replace("/", "."),
                        srcPackagePath,
                        allClasses,
                        allInterfaces);

                promise.complete(packageReport);
            } else {
                promise.fail("The given path isn't a package!");
            }
        });
    }

    @Override
    public Future<ProjectReport> getProjectReport(String srcProjectFolderPath) {
        return vertx.executeBlocking(promise -> {
            File folder = new File(srcProjectFolderPath);
            if (folder.isDirectory()) {
                List<String> folders = new LinkedList<>();
                List<Future> packageFutures = new LinkedList<>();
                List<PackageReport> allPackages = new LinkedList<>();
                List<ClassReport> allClasses = new LinkedList<>();
                List<InterfaceReport> allInterfaces = new LinkedList<>();

                this.collectPackages(folder, folders);
                this.collectFuturePackages(folders, packageFutures);

               CompositeFuture.all(packageFutures).onComplete(res -> {
                    if (res.succeeded()) {
                        allPackages.addAll(res.result().list());
                        allPackages.forEach(packageReport -> {
                            allClasses.addAll(packageReport.getAllClasses());
                            allInterfaces.addAll(packageReport.getAllInterfaces());
                        });

                        ProjectReport projectReport = new ProjectReportImpl(this.findMainClass(allClasses), allInterfaces, allClasses, allPackages);
                        promise.complete(projectReport);
                    } else {
                        promise.fail(res.cause());
                    }
                });
            } else {
                promise.fail("The given path isn't a package!");
            }
        });
    }

    @Override
    public void analyzeProject(String srcProjectFolderPath, String topic) {
        EventBus eventBus = vertx.eventBus();
        File projectFolderPath = new File(srcProjectFolderPath);
        if (projectFolderPath.isDirectory()) {
            List<String> packageFolders = new LinkedList<>();
            this.collectPackages(projectFolderPath, packageFolders);

            for (String packageFolder : packageFolders) {
                List<String> files = new LinkedList<>();
                List<ClassReport> allClasses = new LinkedList<>();
                List<InterfaceReport> allInterfaces = new LinkedList<>();
                this.collectJavaFilesInsideFolder(new File(packageFolder), files);
                files.forEach(classOrInterfacePath -> {
                    try {
                        CompilationUnit cu = StaticJavaParser.parse(new File(classOrInterfacePath));
                        List<String> classOrInterfaceNames = new LinkedList<>();

                        new ClassOrInterfaceCollector().visit(cu, classOrInterfaceNames);

                        classOrInterfaceNames.forEach(name -> {
                            if (cu.getClassByName(name).isPresent()) {
                                try {
                                    cu.getLocalDeclarationFromClassname(name).forEach(classDeclaration -> {
                                        ClassReport classReport = this.getClassInformation(classDeclaration, classOrInterfacePath);
                                        eventBus.publish(topic, classReport.toString());
                                        allClasses.add(classReport);
                                    });
                                } catch (NoSuchElementException e) {
                                    ClassReport classReport = this.getClassInformation(cu.getClassByName(name).get(), classOrInterfacePath);
                                    eventBus.publish(topic, classReport.toString());
                                    allClasses.add(classReport);
                                }
                            } else if (cu.getInterfaceByName(name).isPresent()){
                                InterfaceReport interfaceReport = this.getInterfaceInformation(cu.getInterfaceByName(name).get(), classOrInterfacePath);
                                eventBus.publish(topic, interfaceReport.toString());
                                allInterfaces.add(interfaceReport);
                            }
                        });
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                });
                eventBus.publish(topic, new PackageReportImpl(
                        packageFolder.replaceAll("src/main/java/","").replace("/", "."),
                        packageFolder,
                        allClasses,
                        allInterfaces
                ).toString());
            }
            System.out.println("[ProjectAnalyzer] Finish");
            eventBus.publish(topic, "finish");
        }
    }

    private void  collectFuturePackages(List<String> folders, List<Future> packageFutures){
        for (String pack : folders) {
            packageFutures.add(this.getPackageReport(pack));
        }
    }

    private List<MethodInfo> getMethodsInformation(ClassOrInterfaceDeclaration classOrInterfaceDeclaration, InterfaceReport interfaceReport){
        List<MethodDeclaration> methodDeclarations = classOrInterfaceDeclaration.getMethods();
        List<MethodInfo> methodsInfo = new LinkedList<>();
        methodDeclarations.forEach(method -> methodsInfo.add(new MethodInfoImpl(method.getNameAsString(),
                method.getModifiers().stream().map(Node::toString).collect(Collectors.toList()),
                method.getBegin().get().line,
                method.getEnd().get().line,
                method.getParameters().stream().map(Node::toString).collect(Collectors.toList()),
                interfaceReport)));
        return methodsInfo;
    }

    private List<FieldInfo> getFieldsInformation(ClassOrInterfaceDeclaration classOrInterfaceDeclaration, ClassReport classReport){
        List<List<VariableDeclarator>> fieldDeclarations = classOrInterfaceDeclaration.getFields().stream().map(FieldDeclaration::getVariables).collect(Collectors.toList());
        List<FieldInfo> fieldsInfo = new LinkedList<>();

        fieldDeclarations.forEach( field -> field.forEach(variable -> fieldsInfo.add(new FieldInfoImpl(variable.getNameAsString(), variable.getTypeAsString(), classReport))));
        return fieldsInfo;
    }

    private InterfaceReport getInterfaceInformation(ClassOrInterfaceDeclaration interfaceDeclaration, String srcInterfacePath) throws IllegalArgumentException {
        if (interfaceDeclaration.isInterface()) {
            List<MethodInfo> methodsInfo = new LinkedList<>();
            InterfaceReport interfaceReport = new InterfaceReportImpl(interfaceDeclaration.getFullyQualifiedName().get(), srcInterfacePath, methodsInfo);
            methodsInfo.addAll(this.getMethodsInformation(interfaceDeclaration, interfaceReport));
            return interfaceReport;
        } else {
            throw new IllegalArgumentException("The given path isn't a interface!");
        }
    }

    private ClassReport getClassInformation(ClassOrInterfaceDeclaration classDeclaration, String srcClassPath) throws IllegalArgumentException {
        if (!classDeclaration.isInterface()) {
            List<MethodInfo> methodsInfo = new LinkedList<>();
            List<FieldInfo> fieldsInfo = new LinkedList<>();
            ClassReport classReport = new ClassReportImpl(classDeclaration.getFullyQualifiedName().get(), srcClassPath, methodsInfo, fieldsInfo);

            methodsInfo.addAll(this.getMethodsInformation(classDeclaration, classReport));
            fieldsInfo.addAll(this.getFieldsInformation(classDeclaration, classReport));

            return classReport;
        } else {
            throw new IllegalArgumentException("The given path: " + srcClassPath + " isn't a class!");
        }
    }

    private List<ClassReport> findMainClass(final List<ClassReport> allClasses){
        return allClasses.stream()
                .filter(classReport -> classReport.getMethodsInfo().stream().anyMatch(methodInfo -> methodInfo.getName().equals("main")))
                .collect(Collectors.toList());
    }

    private void collectJavaFilesInsideFolder(final File folder, final List<String> collector) {
        for (final File fileEntry : Objects.requireNonNull(folder.listFiles())) {
            if (!fileEntry.isDirectory() && Files.getFileExtension(fileEntry.getName()).equals("java")) {
                collector.add(fileEntry.getPath());
            }
        }
    }

    private void collectPackages(final File folder, final List<String> collector) {
        collector.add(folder.getPath());
        for (final File fileEntry : Objects.requireNonNull(folder.listFiles())) {
            if (fileEntry.isDirectory()) {
                this.collectPackages(fileEntry, collector);
            }
        }
    }
}
