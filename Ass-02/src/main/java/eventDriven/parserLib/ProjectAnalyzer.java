package eventDriven.parserLib;

import commonElementsProjectAnalyzer.reports.ClassReport;
import commonElementsProjectAnalyzer.reports.InterfaceReport;
import commonElementsProjectAnalyzer.reports.PackageReport;
import commonElementsProjectAnalyzer.reports.ProjectReport;
import io.vertx.core.Future;

import java.util.List;

public interface ProjectAnalyzer {

	/**
	 * Async method to retrieve the report about a specific interface,
	 * given the full path of the interface source file
	 *
	 * @param srcInterfacePath
	 * @return
	 */
	Future<InterfaceReport> getInterfaceReport(String srcInterfacePath);

	/**
	 * Async method to retrieve the report about a list of class,
	 * given the full path of the class source file
	 * 
	 * @param srcClassPath
	 * @return
	 */
	Future<List<ClassReport>> getClassReport(String srcClassPath);

	/**
	 * Async method to retrieve the report about a package,
	 * given the full path of the package folder
	 * 
	 * @param srcPackagePath
	 * @return
	 */
	Future<PackageReport> getPackageReport(String srcPackagePath);

	/**
	 * Async method to retrieve the report about a project
	 * given the full path of the project folder 
	 * 
	 * @param srcProjectFolderPath
	 * @return
	 */
	Future<ProjectReport> getProjectReport(String srcProjectFolderPath);
	
	/**
	 * Async function that analyze a project given the full path of the project folder,
	 * executing the callback each time an event is generated
	 * 
	 * @param srcProjectFolderName
	 * @param topic
	 */
	void analyzeProject(String srcProjectFolderName, String topic);
}
