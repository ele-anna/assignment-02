package reactive.application.controller;

import commonElementsProjectAnalyzer.view.View;
import commonElementsProjectAnalyzer.controller.Controller;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.disposables.Disposable;
import reactive.parserLib.ProjectAnalyzer;

public class ControllerImpl implements Controller {

    private View view;
    private final ProjectAnalyzer analyzer;
    private Disposable disposable;

    public ControllerImpl(ProjectAnalyzer analyzer) {
        this.analyzer = analyzer;
    }

    @Override
    public void setView(View view) {
        this.view = view;
    }

    @Override
    public void startAnalyzeProject(String projectPath) {
        this.log("start analyzer");
        Observable<String> observable = this.analyzer.analyzeProject(projectPath);
        this.disposable = observable.subscribe(this.view::showProjectElem, System.err::println, this.view::notifyFinishComputation);

    }

    @Override
    public void notifyStop() {
        disposable.dispose();
        this.log("stop analyzer");
    }

    private void log(String message) {
        System.out.println("[Controller] " + Thread.currentThread() + message);
    }

}
