package reactive.application;

import commonElementsProjectAnalyzer.controller.Controller;
import reactive.application.controller.ControllerImpl;
import commonElementsProjectAnalyzer.view.View;
import reactive.parserLib.ProjectAnalyzer;
import reactive.parserLib.ProjectAnalyzerImpl;

public class Application {

    public static void main(String[] args) {
        ProjectAnalyzer analyzer = new ProjectAnalyzerImpl();
        Controller controller = new ControllerImpl(analyzer);
        View view = new View(controller, 620, 620);
        controller.setView(view);
        view.display();
    }
}
