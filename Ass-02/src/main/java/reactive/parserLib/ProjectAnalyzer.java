package reactive.parserLib;

import io.reactivex.rxjava3.core.Observable;
import commonElementsProjectAnalyzer.reports.ClassReport;
import commonElementsProjectAnalyzer.reports.InterfaceReport;
import commonElementsProjectAnalyzer.reports.PackageReport;
import commonElementsProjectAnalyzer.reports.ProjectReport;

public interface ProjectAnalyzer {

	/**
	 * Async method to retrieve the report about a specific interface,
	 * given the full path of the interface source file
	 *
	 * @param srcInterfacePath
	 * @return
	 */
	Observable<InterfaceReport> getInterfaceReport(String srcInterfacePath);

	/**
	 * Async method to retrieve the report about a list of class,
	 * given the full path of the class source file
	 * 
	 * @param srcClassPath
	 * @return
	 */
	Observable<ClassReport> getClassReport(String srcClassPath);

	/**
	 * Async method to retrieve the report about a package,
	 * given the full path of the package folder
	 * 
	 * @param srcPackagePath
	 * @return
	 */
	Observable<PackageReport> getPackageReport(String srcPackagePath);

	/**
	 * Async method to retrieve the report about a project
	 * given the full path of the project folder 
	 * 
	 * @param srcProjectFolderPath
	 * @return
	 */
	Observable<ProjectReport> getProjectReport(String srcProjectFolderPath);
	
	/**
	 * Async function that analyze a project given the full path of the project folder,
	 * executing the callback each time an event is generated
	 * 
	 * @param srcProjectFolderName
	 */
	Observable<String> analyzeProject(String srcProjectFolderName);
}
