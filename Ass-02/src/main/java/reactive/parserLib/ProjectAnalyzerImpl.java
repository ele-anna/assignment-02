package reactive.parserLib;

import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.FieldDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.body.VariableDeclarator;
import com.google.common.io.Files;
import io.reactivex.rxjava3.core.Observable;
import commonElementsProjectAnalyzer.collectors.ClassOrInterfaceCollector;
import commonElementsProjectAnalyzer.reports.*;
import commonElementsProjectAnalyzer.reports.implementation.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.stream.Collectors;

public class ProjectAnalyzerImpl implements ProjectAnalyzer {

    @Override
    public Observable<InterfaceReport> getInterfaceReport(String srcInterfacePath) {
        return Observable.create(emitter -> {
            try {
                CompilationUnit cu = StaticJavaParser.parse(new File(srcInterfacePath));
                List<String> interfaceNames = new LinkedList<>();

                new ClassOrInterfaceCollector().visit(cu, interfaceNames);
                InterfaceReport interfaceReport = this.getInterfaceInformation(cu.getInterfaceByName(interfaceNames.get(0)).get(), srcInterfacePath);
                emitter.onNext(interfaceReport);
            } catch (FileNotFoundException | IllegalArgumentException e) {
                e.printStackTrace();
            }
            emitter.onComplete();
        });
    }

    @Override
    public Observable<ClassReport> getClassReport(String srcClassPath) {
        return Observable.create(emitter ->{
            try {
                CompilationUnit cu = StaticJavaParser.parse(new File(srcClassPath));
                List<String> classNames = new LinkedList<>();

                new ClassOrInterfaceCollector().visit(cu, classNames);

                classNames.forEach(name -> {
                    try {
                        cu.getLocalDeclarationFromClassname(name).forEach(classDeclaration -> {
                            emitter.onNext(this.getClassInformation(classDeclaration, srcClassPath));
                        });
                    } catch (NoSuchElementException e) {
                        emitter.onNext(this.getClassInformation(cu.getClassByName(name).get(), srcClassPath));
                    }
                });

            } catch (FileNotFoundException | IllegalArgumentException e) {
                emitter.onError(e);
            }
            emitter.onComplete();
        });
    }

    @Override
    public Observable<PackageReport> getPackageReport(String srcPackagePath) {
        return Observable.create(emitter -> {
            File folder = new File(srcPackagePath);
            if (folder.isDirectory()) {
                List<String> files = new LinkedList<>();
                List<ClassReport> allClasses = new LinkedList<>();
                List<InterfaceReport> allInterfaces = new LinkedList<>();

                this.collectJavaFilesInsideFolder(folder, files);

                files.forEach(classOrInterfacePath -> {
                    try {
                        CompilationUnit cu = StaticJavaParser.parse(new File(classOrInterfacePath));
                        List<String> classOrInterfaceNames = new LinkedList<>();
                        new ClassOrInterfaceCollector().visit(cu, classOrInterfaceNames);

                        classOrInterfaceNames.forEach(name -> {
                            if (cu.getClassByName(name).isPresent()) {
                                try {
                                    cu.getLocalDeclarationFromClassname(name).forEach(classDeclaration -> {
                                        ClassReport classReport = this.getClassInformation(classDeclaration, classOrInterfacePath);
                                        allClasses.add(classReport);
                                    });
                                } catch (NoSuchElementException e) {
                                    ClassReport classReport = this.getClassInformation(cu.getClassByName(name).get(), classOrInterfacePath);
                                    allClasses.add(classReport);
                                }
                            } else if (cu.getInterfaceByName(name).isPresent()){
                                allInterfaces.add(this.getInterfaceInformation(cu.getInterfaceByName(name).get(), classOrInterfacePath));
                            }
                        });
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                });
                PackageReport packageReport = new PackageReportImpl(
                        srcPackagePath.replaceAll("src/main/java/","").replace("/", "."),
                        srcPackagePath,
                        allClasses,
                        allInterfaces);

                emitter.onNext(packageReport);
            } else {
               emitter.onError(new Throwable("The given path isn't a package!"));
            }
            emitter.onComplete();
        });
    }

    @Override
    public Observable<ProjectReport> getProjectReport(String srcProjectFolderPath) {
        return Observable.create(emitter -> {
            File folder = new File(srcProjectFolderPath);
            if (folder.isDirectory()) {
                List<String> folders = new LinkedList<>();
                List<Observable<PackageReport>> packageObservables = new LinkedList<>();
                List<PackageReport> allPackages = new LinkedList<>();
                List<ClassReport> allClasses = new LinkedList<>();
                List<InterfaceReport> allInterfaces = new LinkedList<>();

                this.collectPackages(folder, folders);
                this.collectObservablePackages(folders, packageObservables);

                Observable.merge(packageObservables).subscribe(packageResult -> {
                        allClasses.addAll(packageResult.getAllClasses());
                        allInterfaces.addAll(packageResult.getAllInterfaces());
                        allPackages.add(packageResult);
                    }
                );
                ProjectReport projectReport = new ProjectReportImpl(this.findMainClass(allClasses), allInterfaces, allClasses, allPackages);
                emitter.onNext(projectReport);
            }
            emitter.onComplete();
        });
    }

    @Override
    public Observable<String> analyzeProject(String srcProjectFolderPath) {
        File projectFolderPath = new File(srcProjectFolderPath);
        return Observable.create(emitter -> {
            new Thread(() -> {
                if (projectFolderPath.isDirectory()) {
                    List<String> packageFolders = new LinkedList<>();
                    this.collectPackages(projectFolderPath, packageFolders);

                    for (String packageFolder : packageFolders) {
                        List<String> files = new LinkedList<>();
                        List<ClassReport> allClasses = new LinkedList<>();
                        List<InterfaceReport> allInterfaces = new LinkedList<>();
                        this.collectJavaFilesInsideFolder(new File(packageFolder), files);
                        files.forEach(classOrInterfacePath -> {
                            try {
                                CompilationUnit cu = StaticJavaParser.parse(new File(classOrInterfacePath));
                                List<String> classOrInterfaceNames = new LinkedList<>();

                                new ClassOrInterfaceCollector().visit(cu, classOrInterfaceNames);

                                classOrInterfaceNames.forEach(name -> {
                                    if (cu.getClassByName(name).isPresent()) {
                                        try {
                                            cu.getLocalDeclarationFromClassname(name).forEach(classDeclaration -> {
                                                ClassReport classReport = this.getClassInformation(classDeclaration, classOrInterfacePath);
                                                emitter.onNext(classReport.toString());
                                                allClasses.add(classReport);
                                            });
                                        } catch (NoSuchElementException e) {
                                            ClassReport classReport = this.getClassInformation(cu.getClassByName(name).get(), classOrInterfacePath);
                                            emitter.onNext(classReport.toString());
                                            allClasses.add(classReport);
                                        }
                                    } else if (cu.getInterfaceByName(name).isPresent()){
                                        InterfaceReport interfaceReport = this.getInterfaceInformation(cu.getInterfaceByName(name).get(), classOrInterfacePath);
                                        emitter.onNext(interfaceReport.toString());
                                        allInterfaces.add(interfaceReport);
                                    }
                                });
                            } catch (FileNotFoundException e) {
                                e.printStackTrace();
                            }
                        });
                        emitter.onNext(new PackageReportImpl(
                                packageFolder.replaceAll("src/main/java/","").replace("/", "."),
                                packageFolder,
                                allClasses,
                                allInterfaces
                        ).toString());
                    }
                    System.out.println("[ProjectAnalyzer] Finish");
                    emitter.onComplete();
                }
            }).start();
        });
    }

    private void collectObservablePackages(List<String> folders, List<Observable<PackageReport>> packageFutures){
        for (String pack : folders) {
            packageFutures.add(this.getPackageReport(pack));
        }
    }

    private List<MethodInfo> getMethodsInformation(ClassOrInterfaceDeclaration classOrInterfaceDeclaration, InterfaceReport interfaceReport){
        List<MethodDeclaration> methodDeclarations = classOrInterfaceDeclaration.getMethods();
        List<MethodInfo> methodsInfo = new LinkedList<>();
        methodDeclarations.forEach(method -> methodsInfo.add(new MethodInfoImpl(method.getNameAsString(),
                method.getModifiers().stream().map(Node::toString).collect(Collectors.toList()),
                method.getBegin().get().line,
                method.getEnd().get().line,
                method.getParameters().stream().map(Node::toString).collect(Collectors.toList()),
                interfaceReport)));
        return methodsInfo;
    }

    private List<FieldInfo> getFieldsInformation(ClassOrInterfaceDeclaration classOrInterfaceDeclaration, ClassReport classReport){
        List<List<VariableDeclarator>> fieldDeclarations = classOrInterfaceDeclaration.getFields().stream().map(FieldDeclaration::getVariables).collect(Collectors.toList());
        List<FieldInfo> fieldsInfo = new LinkedList<>();

        fieldDeclarations.forEach( field -> field.forEach(variable -> fieldsInfo.add(new FieldInfoImpl(variable.getNameAsString(), variable.getTypeAsString(), classReport))));
        return fieldsInfo;
    }

    private InterfaceReport getInterfaceInformation(ClassOrInterfaceDeclaration interfaceDeclaration, String srcInterfacePath) throws IllegalArgumentException {
        if (interfaceDeclaration.isInterface()) {
            List<MethodInfo> methodsInfo = new LinkedList<>();
            InterfaceReport interfaceReport = new InterfaceReportImpl(interfaceDeclaration.getFullyQualifiedName().get(), srcInterfacePath, methodsInfo);
            methodsInfo.addAll(this.getMethodsInformation(interfaceDeclaration, interfaceReport));
            return interfaceReport;
        } else {
            throw new IllegalArgumentException("The given path isn't a interface!");
        }
    }

    private ClassReport getClassInformation(ClassOrInterfaceDeclaration classDeclaration, String srcClassPath) throws IllegalArgumentException {
        if (!classDeclaration.isInterface()) {
            List<MethodInfo> methodsInfo = new LinkedList<>();
            List<FieldInfo> fieldsInfo = new LinkedList<>();
            ClassReport classReport = new ClassReportImpl(classDeclaration.getFullyQualifiedName().get(), srcClassPath, methodsInfo, fieldsInfo);

            methodsInfo.addAll(this.getMethodsInformation(classDeclaration, classReport));
            fieldsInfo.addAll(this.getFieldsInformation(classDeclaration, classReport));

            return classReport;
        } else {
            throw new IllegalArgumentException("The given path: " + srcClassPath + " isn't a class!");
        }
    }

    private List<ClassReport> findMainClass(final List<ClassReport> allClasses){
        return allClasses.stream()
                .filter(classReport -> classReport.getMethodsInfo().stream().anyMatch(methodInfo -> methodInfo.getName().equals("main")))
                .collect(Collectors.toList());
    }

    private void collectJavaFilesInsideFolder(final File folder, final List<String> collector) {
        for (final File fileEntry : Objects.requireNonNull(folder.listFiles())) {
            if (!fileEntry.isDirectory() && Files.getFileExtension(fileEntry.getName()).equals("java")) {
                collector.add(fileEntry.getPath());
            }
        }
    }

    private void collectPackages(final File folder, final List<String> collector) {
        collector.add(folder.getPath());
        for (final File fileEntry : Objects.requireNonNull(folder.listFiles())) {
            if (fileEntry.isDirectory()) {
                this.collectPackages(fileEntry, collector);
            }
        }
    }
}
