package commonElementsProjectAnalyzer.view;

import commonElementsProjectAnalyzer.controller.Controller;

public class View {

    private final ViewFrame viewFrame;
    private final Controller controller;

    public View(Controller controller, int width, int height) {
        this.controller = controller;
        this.viewFrame = new ViewFrame(this, width, height);
    }

    public void display() {
        this.viewFrame.display();
    }


    public void notifyStartAnalyzeProject(String projectPath) {
        this.controller.startAnalyzeProject(projectPath);
    }

    public void showProjectElem(String projectReport) {
        this.viewFrame.setProjectElement(projectReport);
    }

    public void notifyStop() {
        this.controller.notifyStop();
    }

    public void notifyFinishComputation() {
        this.viewFrame.notifyFinishComputation();
    }
}
