package commonElementsProjectAnalyzer.view;

import javax.swing.*;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class ViewFrame extends JFrame implements ActionListener {

    private final JTextField projectField;
    private final View view;
    private final JTextArea analyzingResult;
    private final JButton stopButton;
    private final JButton parseButton;
    private final JButton browseButton;

    public ViewFrame(View view, int width, int height) {
        super("Project Analyzer Application");
        this.view = view;
        super.setSize(width, height);

        JPanel selectionPanel = new JPanel();
        this.projectField = new JTextField(30);
        this.browseButton = new JButton("Browse");
        this.parseButton = new JButton("Analyze");
        selectionPanel.add(new JLabel("Select a project:"));
        selectionPanel.add(this.projectField);
        selectionPanel.add(this.browseButton);
        selectionPanel.add(this.parseButton);

        JPanel projectReportPanel = new JPanel();
        projectReportPanel.setBorder(new TitledBorder(new EtchedBorder(), "Project Report: "));
        this.analyzingResult = new JTextArea(30, 60);
        this.analyzingResult.setEditable(false);
        JScrollPane scrollPane = new JScrollPane(this.analyzingResult);
        scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        projectReportPanel.add(scrollPane);

        JPanel commandPanel = new JPanel();
        this.stopButton = new JButton("stop");
        commandPanel.add(this.stopButton);

        this.getContentPane().add(BorderLayout.NORTH, selectionPanel);
        this.getContentPane().add(BorderLayout.CENTER, projectReportPanel);
        this.getContentPane().add(BorderLayout.SOUTH, commandPanel);
        this.pack();

        browseButton.addActionListener(this);
        parseButton.addActionListener(this);
        this.stopButton.addActionListener(this);

        addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent ev){
                System.exit(-1);
            }
            public void windowClosed(WindowEvent ev){
                System.exit(-1);
            }
        });
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand();
        switch (command) {
            case "Browse":
                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                int state = fileChooser.showOpenDialog(null);
                if (state == JFileChooser.APPROVE_OPTION) {
                    this.projectField.setText(fileChooser.getSelectedFile().getAbsolutePath());
                }
                break;
            case "Analyze":
                String projectPath = this.projectField.getText();
                if (!projectPath.isBlank()) {
                    this.setAnalyzingStatus(true);
                    this.clearAnalyzingResult();
                    this.view.notifyStartAnalyzeProject(projectPath);
                }
                break;
            case "stop":
                this.setAnalyzingStatus(false);
                this.view.notifyStop();
                break;
        }
    }

    public void setProjectElement(String projectElement) {
        SwingUtilities.invokeLater(() -> {
            if (this.analyzingResult.getText().isBlank()) {
                this.analyzingResult.setText(projectElement);
            } else {
                this.analyzingResult.append("\n" + projectElement);
            }
        });
    }

    public void display() {
        SwingUtilities.invokeLater(() -> this.setVisible(true));
    }

    public void notifyFinishComputation() {
        this.setAnalyzingStatus(false);
    }

    private void clearAnalyzingResult() {
        SwingUtilities.invokeLater(() -> {
            if (!this.analyzingResult.getText().isBlank()) {
                this.analyzingResult.setText("");
            }
        });
    }

    private void setAnalyzingStatus(boolean isAnalyzing) {
        SwingUtilities.invokeLater(() -> {
            this.stopButton.setEnabled(isAnalyzing);
            this.parseButton.setEnabled(!isAnalyzing);
            this.browseButton.setEnabled(!isAnalyzing);
        });
    }
}
