package commonElementsProjectAnalyzer.reports;

import java.util.List;

public interface ProjectReport {

	List<ClassReport> getMainClass();

	List<InterfaceReport> getAllInterfaces();
	
	List<ClassReport> getAllClasses();
	
	List<PackageReport> getAllPackages();
}
