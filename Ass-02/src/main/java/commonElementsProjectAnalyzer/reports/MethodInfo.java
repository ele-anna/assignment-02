package commonElementsProjectAnalyzer.reports;

import java.util.List;

public interface MethodInfo {

	String getName();

	int getSrcBeginLine();

	int getEndBeginLine();

	List<String> getModifiers();

	List<String> getParameters();

	InterfaceReport getParent();


}
