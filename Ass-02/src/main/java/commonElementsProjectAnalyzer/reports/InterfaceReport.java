package commonElementsProjectAnalyzer.reports;

import java.util.List;

public interface InterfaceReport {

    String getFullName();

    String getSrcFullFileName();

    List<MethodInfo> getMethodsInfo();

}
