package commonElementsProjectAnalyzer.reports;

import java.util.List;

public interface PackageReport {

	String getPackageName();
	
	String getSrcFullFileName();

	List<ClassReport> getAllClasses();

	List<InterfaceReport> getAllInterfaces();
	
}
