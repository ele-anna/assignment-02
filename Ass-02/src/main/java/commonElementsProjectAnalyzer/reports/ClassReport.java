package commonElementsProjectAnalyzer.reports;

import java.util.List;

public interface ClassReport extends InterfaceReport {

	List<FieldInfo> getFieldsInfo();
	
}
