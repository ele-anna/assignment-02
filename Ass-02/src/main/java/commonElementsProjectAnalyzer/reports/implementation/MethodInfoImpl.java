package commonElementsProjectAnalyzer.reports.implementation;

import commonElementsProjectAnalyzer.reports.InterfaceReport;
import commonElementsProjectAnalyzer.reports.MethodInfo;

import java.util.List;

public class MethodInfoImpl implements MethodInfo {

    private final String name;
    private final List<String> modifiers;
    private final int srcBeginLine;
    private final int srcEndBeginLine;
    private final List<String> parameters;
    private final InterfaceReport parent;

    public MethodInfoImpl(String name, List<String> modifiers, int srcBeginLine, int srcEndBeginLine, List<String> parameters, InterfaceReport parent) {
        this.name = name;
        this.modifiers = modifiers;
        this.srcBeginLine = srcBeginLine;
        this.srcEndBeginLine = srcEndBeginLine;
        this.parameters = parameters;
        this.parent = parent;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public int getSrcBeginLine() {
        return this.srcBeginLine;
    }

    @Override
    public int getEndBeginLine() {
        return this.srcEndBeginLine;
    }

    @Override
    public List<String> getModifiers() {
        return this.modifiers;
    }

    @Override
    public List<String> getParameters() {
        return this.parameters;
    }

    @Override
    public InterfaceReport getParent() {
        return this.parent;
    }

    @Override
    public String toString() {
        return "Method information:" + "\n"+
                "\t\t\tname='" + name + '\'' + "\n" +
                "\t\t\tscopeDeclaration='" + modifiers + '\'' + "\n" +
                "\t\t\tsrcBeginLine=" + srcBeginLine + "\n" +
                "\t\t\tsrcEndBeginLine=" + srcEndBeginLine + "\n" +
                "\t\t\tparameters=" + parameters + "\n";
    }
}
