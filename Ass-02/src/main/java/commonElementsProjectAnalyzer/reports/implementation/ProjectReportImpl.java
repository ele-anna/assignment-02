package commonElementsProjectAnalyzer.reports.implementation;

import commonElementsProjectAnalyzer.reports.ClassReport;
import commonElementsProjectAnalyzer.reports.InterfaceReport;
import commonElementsProjectAnalyzer.reports.PackageReport;
import commonElementsProjectAnalyzer.reports.ProjectReport;

import java.util.List;
import java.util.stream.Collectors;

public class ProjectReportImpl implements ProjectReport {

    private final List<ClassReport> mainClass;
    private final List<InterfaceReport> allInterfaces;
    private final List<ClassReport> allClasses;
    private final List<PackageReport> allPackages;

    public ProjectReportImpl(List<ClassReport> mainClass, List<InterfaceReport> allInterfaces, List<ClassReport> allClasses, List<PackageReport> allPackages) {
        this.mainClass = mainClass;
        this.allInterfaces = allInterfaces;
        this.allClasses = allClasses;
        this.allPackages = allPackages;
    }

    @Override
    public List<ClassReport> getMainClass() {
        return this.mainClass;
    }

    @Override
    public List<InterfaceReport> getAllInterfaces() {
        return this.allInterfaces;
    }

    @Override
    public List<ClassReport> getAllClasses() {
        return this.allClasses;
    }

    @Override
    public List<PackageReport> getAllPackages() {
        return this.allPackages;
    }

    @Override
    public String toString() {
        return "Project Report: " + "\n" +
                "\tmainClass: \n\t\t" + mainClass.stream().map(ClassReport::getFullName).collect(Collectors.joining("\n\t\t"))  +
                "\n\tall interface: \n\t\t" + this.allInterfaces.stream().map(InterfaceReport::getFullName).collect(Collectors.joining("\n\t\t")) +
                "\n\tall classes: \n\t\t" + this.allClasses.stream().map(ClassReport::getFullName).collect(Collectors.joining("\n\t\t")) +
                "\n\tall packages: \n\t\t" + this.allPackages.stream().map(PackageReport::getPackageName).collect(Collectors.joining("\n\t\t"));
    }
}
