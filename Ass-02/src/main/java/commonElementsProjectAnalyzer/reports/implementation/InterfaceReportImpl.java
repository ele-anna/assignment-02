package commonElementsProjectAnalyzer.reports.implementation;

import commonElementsProjectAnalyzer.reports.InterfaceReport;
import commonElementsProjectAnalyzer.reports.MethodInfo;

import java.util.List;
import java.util.stream.Collectors;

public class InterfaceReportImpl implements InterfaceReport {

    private final String fullName;
    private final String srcFullFileName;
    private final List<MethodInfo> methodsInfo;

    public InterfaceReportImpl(String fullName, String srcFullFileName, List<MethodInfo> methodsInfo) {
        this.fullName = fullName;
        this.srcFullFileName = srcFullFileName;
        this.methodsInfo = methodsInfo;
    }

    @Override
    public String getFullName() {
        return this.fullName;
    }

    @Override
    public String getSrcFullFileName() {
        return this.srcFullFileName;
    }

    @Override
    public List<MethodInfo> getMethodsInfo() {
        return this.methodsInfo;
    }

    @Override
    public String toString() {
        return "Interface Report: " + "\n" +
                "\tfull name='" + fullName + '\'' + "\n" +
                "\tsrc full file name='" + srcFullFileName + '\'' + "\n" +
                "\tmethods: \n"+ this.getMethodsInfo().stream().map(MethodInfo::toString).collect(Collectors.joining("\n\t\t", "\t\t", ""));
    }
}
