package commonElementsProjectAnalyzer.reports.implementation;

import commonElementsProjectAnalyzer.reports.ClassReport;
import commonElementsProjectAnalyzer.reports.InterfaceReport;
import commonElementsProjectAnalyzer.reports.PackageReport;

import java.util.List;
import java.util.stream.Collectors;

public class PackageReportImpl implements PackageReport {

    private final String packageName;
    private final String srcFullFileName;
    private final List<ClassReport> allClasses;
    private final List<InterfaceReport> allInterfaces;

    public PackageReportImpl(String packageName, String srcFullFileName, List<ClassReport> allClasses, List<InterfaceReport> allInterfaces) {
        this.packageName = packageName;
        this.srcFullFileName = srcFullFileName;
        this.allClasses = allClasses;
        this.allInterfaces = allInterfaces;
    }

    @Override
    public String getPackageName() {
        return this.packageName;
    }

    @Override
    public String getSrcFullFileName() {
        return this.srcFullFileName;
    }

    @Override
    public List<ClassReport> getAllClasses() {
        return this.allClasses;
    }

    @Override
    public List<InterfaceReport> getAllInterfaces() {
        return this.allInterfaces;
    }

    @Override
    public String toString() {
        return "Package Report:" + "\n" +
                "\tpackage name='" + packageName + '\'' + "\n" +
                "\tsrc full file name='" + srcFullFileName + '\'' + "\n"+
                "\tall classes: \n" + this.allClasses.stream().map(ClassReport::getFullName).collect(Collectors.joining("\n\t\t", "\t\t", "\n")) +
                "\tall interfaces: \n" + this.allInterfaces.stream().map(InterfaceReport::getFullName).collect(Collectors.joining("\n\t\t", "\t\t", ""));
    }
}
