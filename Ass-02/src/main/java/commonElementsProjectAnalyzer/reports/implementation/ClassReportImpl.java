package commonElementsProjectAnalyzer.reports.implementation;

import commonElementsProjectAnalyzer.reports.ClassReport;
import commonElementsProjectAnalyzer.reports.FieldInfo;
import commonElementsProjectAnalyzer.reports.MethodInfo;

import java.util.List;
import java.util.stream.Collectors;

public class ClassReportImpl  extends InterfaceReportImpl implements ClassReport {

    private final List<FieldInfo> fieldsInfo;

    public ClassReportImpl(String className, String fileName, List<MethodInfo> methodsInfo, List<FieldInfo> fieldsInfo) {
        super(className, fileName, methodsInfo);
        this.fieldsInfo = fieldsInfo;
    }

    @Override
    public List<FieldInfo> getFieldsInfo() {
        return this.fieldsInfo;
    }

    @Override
    public String toString() {
        return "Class Report: " + "\n" +
                "\tclass name='" + super.getFullName() + '\'' + "\n" +
                "\tfile name='" + super.getSrcFullFileName() + '\'' + "\n" +
                "\tfields: \n" + this.getFieldsInfo().stream().map(FieldInfo::toString).collect(Collectors.joining("\n\t\t", "\t\t", "\n")) +
                "\tmethods: \n"+ this.getMethodsInfo().stream().map(MethodInfo::toString).collect(Collectors.joining("\n\t\t", "\t\t", ""));
    }
}
