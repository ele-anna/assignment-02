package commonElementsProjectAnalyzer.reports.implementation;

import commonElementsProjectAnalyzer.reports.ClassReport;
import commonElementsProjectAnalyzer.reports.FieldInfo;

public class FieldInfoImpl implements FieldInfo {

    private final String name;
    private final String fieldTypeFullName;
    private final ClassReport parent;

    public FieldInfoImpl(String name, String fieldTypeFullName, ClassReport parent) {
        this.name = name;
        this.fieldTypeFullName = fieldTypeFullName;
        this.parent = parent;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getFieldTypeFullName() {
        return this.fieldTypeFullName;
    }

    @Override
    public ClassReport getParent() {
        return this.parent;
    }

    @Override
    public String toString() {
        return "Field information:" + "\n" +
                "\t\t\tname='" + name + '\'' + "\n" +
                "\t\t\tfieldTypeFullName='" + fieldTypeFullName + "'";
    }
}
