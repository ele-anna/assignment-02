package commonElementsProjectAnalyzer.reports;

public interface FieldInfo {

	String getName();
	
	String getFieldTypeFullName();
	
	ClassReport getParent();
}
