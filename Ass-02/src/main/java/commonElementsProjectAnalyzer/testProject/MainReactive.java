package commonElementsProjectAnalyzer.testProject;

import reactive.parserLib.ProjectAnalyzer;
import reactive.parserLib.ProjectAnalyzerImpl;

public class MainReactive {

    public static void main(String[] args){
        System.out.println("[main] start");
        ProjectAnalyzer analyzer = new ProjectAnalyzerImpl();

        analyzer.getClassReport("src/main/java/commonElementsProjectAnalyzer/testProject/MyClass.java")
                .subscribe(System.out::println);

        analyzer.getInterfaceReport("src/main/java/commonElementsProjectAnalyzer/testProject/MyInterface.java")
                .subscribe(System.out::println);

        analyzer.getPackageReport("src/main/java/commonElementsProjectAnalyzer/testProject/myPackage")
                .subscribe(System.out::println);

        analyzer.getProjectReport("src/main/java/commonElementsProjectAnalyzer/testProject")
                .subscribe(System.out::println);

        System.out.println("[main] done");
    }
}
