package commonElementsProjectAnalyzer.testProject;

import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.PackageDeclaration;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.FieldDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

class MethodNameCollector extends VoidVisitorAdapter<List<String>> {
    public void visit(MethodDeclaration md, List<String> collector) {
        super.visit(md, collector);
        collector.add(md.getNameAsString());
    }
}

class FullCollectorTest extends VoidVisitorAdapter<Void> {

    public void visit(PackageDeclaration fd, Void collector) {
        super.visit(fd, collector);
        System.out.println(fd);
    }

    public void visit(ClassOrInterfaceDeclaration cd, Void collector) {
        super.visit(cd, collector);
        System.out.println(cd.getNameAsString());
    }

    public void visit(FieldDeclaration fd, Void collector) {
        super.visit(fd, collector);
        System.out.println(fd);
    }

    public void visit(MethodDeclaration md, Void collector) {
        super.visit(md, collector);
        System.out.println(md.getName());
    }
}


public class TestJavaParser {

    public static void main(String[] args) throws Exception {
        CompilationUnit cu = StaticJavaParser.parse(new File("src/main/java/commonElementsProjectAnalyzer/testProject/TestJavaParser.java"));

        ArrayList methodNames = new ArrayList<String>();
        MethodNameCollector methodNameCollector = new MethodNameCollector();
        methodNameCollector.visit(cu,methodNames);
        methodNames.forEach(n -> System.out.println("MethodNameCollected:" + n));
    }
}
