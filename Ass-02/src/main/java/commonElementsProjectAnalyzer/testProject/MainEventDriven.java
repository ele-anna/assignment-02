package commonElementsProjectAnalyzer.testProject;

import commonElementsProjectAnalyzer.reports.ClassReport;
import commonElementsProjectAnalyzer.reports.InterfaceReport;
import commonElementsProjectAnalyzer.reports.PackageReport;
import commonElementsProjectAnalyzer.reports.ProjectReport;
import eventDriven.parserLib.ProjectAnalyzer;
import eventDriven.parserLib.ProjectAnalyzerImpl;
import io.vertx.core.Future;
import io.vertx.core.Vertx;

import java.util.List;

public class MainEventDriven {

    public static void main(String[] args){
        Vertx vertx = Vertx.vertx();
        ProjectAnalyzer analyzer = new ProjectAnalyzerImpl(vertx);

        Future<List<ClassReport>> resultClass =  analyzer.getClassReport("src/main/java/commonElementsProjectAnalyzer/testProject/MyClass.java");
        resultClass.onSuccess(res -> res.forEach(System.out::println)).onFailure(System.err::println);

        Future<InterfaceReport> resultInterface =  analyzer.getInterfaceReport("src/main/java/commonElementsProjectAnalyzer/testProject/MyInterface.java");
        resultInterface.onSuccess(System.out::println).onFailure(System.err::println);

        Future<PackageReport> resultPackage =  analyzer.getPackageReport("src/main/java/commonElementsProjectAnalyzer/testProject/myPackage");
        resultPackage.onSuccess(System.out::println).onFailure(System.err::println);

        Future<ProjectReport> resultProject = analyzer.getProjectReport("src/main/java/commonElementsProjectAnalyzer/testProject");
        resultProject.onSuccess(System.out::println).onFailure(System.err::println);

    }
}
