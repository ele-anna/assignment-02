package commonElementsProjectAnalyzer.testProject;

public class MyClassWithNestedClasses {

    private int field;

    public void method(){}

    public static class MyHiddenClass{

    }
}

class MyNestedClass {

    private int nestedField;

    private void nestedMethod() {}
}
