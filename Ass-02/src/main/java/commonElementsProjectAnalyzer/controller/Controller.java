package commonElementsProjectAnalyzer.controller;

import commonElementsProjectAnalyzer.view.View;

public interface Controller {

    void setView(View view);

    void startAnalyzeProject(String projectPath);

    void notifyStop();
}
